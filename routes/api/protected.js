const express = require('express');
const router = express.Router();

const User = require('../../models/User');

// @route GET api/protected/{:id}/profile
// @desc Get verything stored in db for user
// @access Private
router.get("/:id/profile", (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (err) return next(err);
        res.send(user);
    })
});

// @route POST api/protected/update
// @desc Update user personal info & lift maxes
// @access Private
router.post("/update", (req, res) => {
    User.findByIdAndUpdate(req.body.id, {$set: req.body}, (err, user) => {
        if (err) return next(err);
        res.send(req.body);
    });
});

// @route GET api/protected/{:id}/lifts
// @desc Get all lifts associated with the user
// @access Private
router.get("/:id/lifts", (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (err) return next (err);
        res.send(user.lifts);
    });
});

// @route GET api/protected/{:id}/personal
// @desc Get all personal info associated with the user
// @access Private
router.get("/:id/personal", (req, res) => {
    User.findById(req.params.id, (err, user) => {
        if (err) return next (err);
        res.send(user.personal);
    });
});

module.exports = router;