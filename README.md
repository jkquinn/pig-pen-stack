## Installed Packages

`bcrypt`: has passwords before storing in a database<br>
`body-parser`: parse incoming request bodies<br>
`concurrently`: start scripts<br> 
`express`: sits on top of node to make routing, request handling, and responding easier to write<br>
`is-empty`: global function that will come in handy when using validator<br>
`jsonwebtoken`: used for authorization JSON. Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties.<br>
`mongoose`: used to interact with MongoDB<br>
`passport`: authenticate requests, done through strategies<br>
`passport-jwt`: passport strategy for authenticating with a json web token<br>
`validator`: used to validate inputs

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the client in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

Also runs the server in the development mode.<br>
The client will make requests to the server on localhost:5000