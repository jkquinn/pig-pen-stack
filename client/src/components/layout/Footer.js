import React from 'react';

const Footer = () => {
    return(
        <footer className="footer-copyright black">
            <div className="container white-text">
                <b><i>Est. 2016</i></b>
                <a className="white-text right" href="http://instagram.com/senor_puro">
                    <i className="material-icons">photo_camera</i>
                </a>
            </div>
        </footer>
    );
}

export default Footer;