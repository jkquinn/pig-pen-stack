const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        default: Date.now,
    },
    lifts: {
        squat: Number,
        bench: Number,
        deadlift: Number,
    },
    personal: {
        age: Number,
        height: {
            feet: Number,
            inches: Number,
        },
        weight: Number,
        gender: String,
    },
});

module.exports = User = mongoose.model("users", UserSchema);